package utils;

import java.util.List;

import org.apache.kafka.common.requests.MetadataResponse.PartitionMetadata;
import org.apache.kafka.common.requests.MetadataResponse.TopicMetadata;
import org.apache.kafka.common.security.JaasUtils;

import kafka.admin.AdminUtils;
import kafka.utils.ZkUtils;

public class TopicUtil {

	public static int getTopicPartitionNum(String topic) {

		int num;
		ZkUtils zkUtils = ZkUtils.apply("192.168.174.129:2181", 30000, 30000,
				JaasUtils.isZkSecurityEnabled());

		TopicMetadata props1 = AdminUtils.fetchTopicMetadataFromZk(topic, zkUtils);

		List<PartitionMetadata> list = props1.partitionMetadata();
		num = list.size();
		System.out.println("num:"+num);
		zkUtils.close();
		return num;

	}

}
