package kafka.consumer;

import java.util.ArrayList;

import utils.TopicUtil;

public class ConsumerTestMain {

	public static void main(String[] args) {

		int PARTITIONS;
		String topic = args[0];
		long offset = Long.parseLong(args[1]);
		PARTITIONS = TopicUtil.getTopicPartitionNum(topic);

		ArrayList<ConsumerTest> ct = new ArrayList<ConsumerTest>();
		for (int i = 0; i < PARTITIONS; i++) {
			ct.add(new ConsumerTest("Thread-" + i, topic, i, offset));
		}

		for (int i = 0; i < PARTITIONS; i++) {
			ct.get(i).start();
		}

	}
}
