package kafka.consumer;

import java.util.ArrayList;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

public class ConsumerTest implements Runnable {
	private final KafkaConsumer<String, String> consumer;
	String threadName;
	String topicname;
	Thread thread;
	int partitionNum;
	long offset;
	private TopicPartition tp;
	ConsumerTest consumerTest;

	ConsumerTest(String threadName, String topicName, int partitionNum, long offset) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.174.129:9092");
		props.put("group.id", "test");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

		this.threadName = threadName;
		this.topicname = topicName;
		this.partitionNum = partitionNum;
		this.offset = offset;
		consumer = new KafkaConsumer<>(props);
	}

	@Override
	public void run() {
		tp = new TopicPartition(topicname, partitionNum);
		consumerTest = new ConsumerTest(threadName, topicname, partitionNum, offset);

		ArrayList<TopicPartition> list = new ArrayList<>();
		list.add(tp);
		consumerTest.consumer.assign(list);
		consumerTest.consumer.seek(tp, offset);
		while (true) {
			ConsumerRecords<String, String> records = consumerTest.consumer.poll(100);
			for (ConsumerRecord<String, String> record : records) {
				System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(),
						record.key(), record.value());
			}
			consumerTest.consumer.commitSync();
		}

	}

	public void start() {
		System.out.println("Starting Thread : " + threadName);
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
}