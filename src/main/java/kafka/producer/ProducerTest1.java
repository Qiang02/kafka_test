package kafka.producer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class ProducerTest1 implements Runnable {

	String topic;
	int partitionNum;
	Thread thread;

	static KafkaProducer<String, String> producer = createProducer();

	public ProducerTest1(String topic, int partition) {
		super();
		this.topic = topic;
		this.partitionNum = partition;
	}

	@Override
	public void run() {

		if (partitionNum == 0) {
			for (int i = 1; i < 100; i++) {
				try {
					producer.send(
							new ProducerRecord<String, String>(topic, partitionNum, null, i + ""))
							.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		}
		if (partitionNum == 1) {
			for (char i = 'a'; i <= 'z'; i++) {
				try {
					producer.send(
							new ProducerRecord<String, String>(topic, partitionNum, null, i + ""))
							.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.174.129:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		return new KafkaProducer<String, String>(props);
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

}
