package kafka.producer;

import java.util.ArrayList;

import utils.TopicUtil;

public class ProducerTestMain1 {

	public static void main(String[] args) {

		int PARTITIONS;
		String topicname = "xxoo";
		PARTITIONS = TopicUtil.getTopicPartitionNum(topicname);

		ArrayList<ProducerTest1> kafka = new ArrayList<ProducerTest1>();
		for (int i = 0; i < PARTITIONS; i++) {
			kafka.add(new ProducerTest1(topicname, i));
		}

		for (int i = 0; i < PARTITIONS; i++) {
			kafka.get(i).start();
		}

	}
}
