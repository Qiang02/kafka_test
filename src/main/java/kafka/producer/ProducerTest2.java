package kafka.producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class ProducerTest2 {

	String topic;
	
	static KafkaProducer<String, String> producer = createProducer();

	public ProducerTest2(String topic) {
		super();
		this.topic = topic;
	}

	public void liqiang() {
		
//			for (int i = 1; i < 5; i++) {
				producer.send(new ProducerRecord<String, String>(topic, 0, null, "aaa"));
//			}
//			for(char i = 'a'; i <= 'z'; i++){
//				producer.send(new ProducerRecord<String, String>(topic, 1,null, i+""));
//			}
	}

	private static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.174.129:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		return new KafkaProducer<String, String>(props);
	}

}
